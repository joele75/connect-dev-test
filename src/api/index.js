import 'isomorphic-fetch'

/**
* Swapped out with fetch for brevity
**/

export const getData = (cb) => {
    return fetch('http://localhost:3020/api/vehicle')
        .then(response => response.json() )
        .then(json => json.vehicles)
        .catch(e => console.log.bind(e));
};

export const getItem = id => {
	return fetch(`http://localhost:3020/api/vehicle/${id}`)
        .then(response => response.json())
        .catch(e => console.log.bind(e));
};