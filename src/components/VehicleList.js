import React, { Component } from 'react';
import Vehicle from './Vehicle';
import { getData, getItem } from '../api';

export default class VehicleList extends Component {

	constructor(props) {
		super(props);

		this.state = {
			data: []
		}
	}

	componentDidMount() {
		getData().then(data => {
            let requests = data.map(item => getItem(item.id));
            // When all requests are completed, merge the two arrays so we have the full data set
			// and set it to the component state
			let vehicles = [];
			Promise.all(requests).then(full_data => {
				data.forEach((vehicle) => {
                    vehicle = Object.assign(vehicle, full_data.find(el => el.id === vehicle.id));
                    vehicles.push({
                        key: vehicle.id,
                        name: 'Jaguar ' + vehicle.id,
                        price: 'From ' + vehicle.price,
						description: vehicle.description,
						image: vehicle.media[0] !== undefined ? vehicle.media[0].url : ''
                    });
				});
                this.setState({
					data: vehicles
				});
            });
        });
	}

	render() {
		if(this.state.data) {
		    return (
		    	<div className="vehicle-list">
					{ this.state.data.map(vehicle => (
						<Vehicle {...vehicle}/>
					))}
				</div>
		    )
	    }
		return (<h1>Loading data...</h1>);
	}
}