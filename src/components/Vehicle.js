import React, { Component } from 'react';

export default class Vehicle extends Component {
	constructor(props) {
		super(props);
	}
    render() {
        return (
			<div className="vehicle-list-item">
				<div className="vehicle-list-item__image" style={{ backgroundImage: `url(${this.props.image})`}}>
					<div className="image"></div>
				</div>
				<div className="vehicle-list-item__text">
					<div className="name">{this.props.name}</div>
					<div className="price">{this.props.price}</div>
					<div className="description">{this.props.description}</div>
				</div>
			</div>
        )
    }
};