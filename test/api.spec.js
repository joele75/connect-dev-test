/**
 * Updated test files
 */

import chai, {assert, expect} from 'chai';
import {getData, getItem} from '../src/api';
import server from '../server';


describe("API tests", function() {
    let s;
    beforeEach(() => {
        s = server.listen(3020);
    });

    afterEach(function () {
        s.close();
    });

    it('should respond with an array of vehicles', (done) => {
        getData().then(data => {
            assert.isArray(data);
            done();
        }).catch(e => {
            done(e);
        });
    });

    it('should respond with a vehicle object with keys [id, description, meta, price]', (done) => {
        getItem('xf').then(item => {
            assert.isObject(item);
            expect(item).to.have.all.keys(['id', 'description', 'meta', 'price']);
            done();
        }).catch(e => {
            done(e);
        });
    });
});