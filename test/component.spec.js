/**
 * Updated test files
 */

import chai, {assert, expect} from 'chai';
import server from '../server';
import jsxChai from 'jsx-chai';
import React from 'react';
import TestUtils from 'react-dom/test-utils'
import Vehicle from '../src/components/Vehicle';

chai.use(jsxChai);

describe("Component tests", function() {

    it('should render and the containing element should be a div tag', (done) => {
        const renderer = TestUtils.createRenderer();
        renderer.render(<Vehicle />);
        const actual = renderer.getRenderOutput().type;
        const expected = 'div';
        expect(actual).to.equal(expected);
        done();
    });

});
